﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System;
using UnityEngine.UI;

public class CardDataInput : MonoBehaviour
{
    public struct ALLCard
    {
        // カードのIDを保持
        public int id;

        // カードの名前
        public string card_name;

        // カードの詳細を保持
        public string card_flavor;

        // カードのタイプを保持
        public int card_type;

        //カードの効果タイプを保持
        public int effect_type;

        //カードの効果値を保持
        public int effect_rate;

        //カードのエフェクトを保持
        public string image_effect;

        //カードの画像パスを保持
        public string image_path;

        public ALLCard(int id, string card_name, string card_flavor, int card_type, int effect_type, int effect_rate, string image_effect, string image_path)
        {
            this.id = id;
            this.card_name = card_name;
            this.card_flavor = card_flavor;
            this.card_type = card_type;
            this.effect_type = effect_type;
            this.effect_rate = effect_rate;
            this.image_effect = image_effect;
            this.image_path = image_path;
        }
    }

    //マスターデータ参照
    public string filePass;

    // カードを保持するリスト(all)
    List<ALLCard> allcardList;

    // カードを保持するリスト(deck)
    List<ALLCard> cardList;

    // 各マーク(スート)での枚数を設定
    const int Allcard_count_max = 30;

    // 各マーク(スート)のIDを決める
    const int Spade = 0;

    void Start()
    {
        Loadmaster();
       // InitializeCardList();
        ShowCardsName();
    }

    void Loadmaster()
    {
        int i = 0, j;
        TextAsset csv = Resources.Load("CSV/" + filePass) as TextAsset;
        StringReader reader = new StringReader(csv.text);
        while (reader.Peek() > -1)
        {
            string line = reader.ReadLine();
            string [] values = line.Split(',');
            for (j = 0; j < values.Length; j++)
            {
                ALLCard allcard = new ALLCard(int.Parse(values[0]), values[1], values[2], int.Parse(values[3]), int.Parse(values[4]), int.Parse(values[5]), values[6], values[7]);
                allcardList.Add(allcard);
            }
            i++;

        }

    }

    //void InitializeCardList()
    //{
    //    // 山札を初期化する int id, string card_name, string card_flavor, int card_type, int effect_type, int effect_rate, string image_effect, string image_path
    //    cardList = new List<ALLCard>();
    //    for (int i = 1; i <= Allcard_count_max; i++)
    //    {
    //        Card card = new ALLCard(i, cardname, flavor,type,effecttype,effectrate,imageeffect,cardfile);
    //        cardList.Add(card);
    //    }
    //}

    void ShowCardsName()
    {
        // コンソールに出力する文字列を生成
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        foreach (ALLCard card in allcardList)
        {
            sb.Append(GetSuitMark(card.id)).Append(", ");
        }
        string cardText = sb.ToString();
        // 最後のカンマだけ削除する
        cardText = cardText.Remove(cardText.LastIndexOf(", "));
        Debug.Log(cardText);
    }

    string GetSuitMark(int suit)
    {
        // マーク(スート)のIDに応じた文字を返す
        string CardName;
        switch (suit)
        {
            case 10001:
                CardName = "ATK+1";
                break;
            case 10002:
                CardName = "ATK+2";
                break;
            case 10003:
                CardName = "ATK+3";
                break;
            case 20001:
                CardName = "DEF+1";
                break;
            case 20002:
                CardName = "DEF+2";
                break;
            case 20003:
                CardName = "DEF+3";
                break;
            case 30001:
                CardName = "SPD+1";
                break;
            case 30002:
                CardName = "SPD+2";
                break;
            case 30003:
                CardName = "SPD+3";
                break;

            default:
                CardName = "JOKER";
                break;
        }
        return CardName;
    }
}
