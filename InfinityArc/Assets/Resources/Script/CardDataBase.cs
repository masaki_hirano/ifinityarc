﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardDataBase : MonoBehaviour {

    public struct ALLCard
    {
        // カードのIDを保持
        public int id;

        // カードの名前
        public string card_name;

        // カードの詳細を保持
        public string card_flavor;

        // カードのタイプを保持
        public int card_type;

        //カードの効果タイプを保持
        public int effect_type;

        //カードの効果値を保持
        public int effect_rate;

        //カードのエフェクトを保持
        public string image_effect;

        //カードの画像パスを保持
        public string image_path;

        public ALLCard(int id, string card_name, string card_flavor, int card_type, int effect_type, int effect_rate, string image_effect,string image_path)
        {
            this.id = id;
            this.card_name = card_name;
            this.card_flavor = card_flavor;
            this.card_type = card_type;
            this.effect_type = effect_type;
            this.effect_rate = effect_rate;
            this.image_effect = image_effect;
            this.image_path = image_path; 
        }
    }
}
