﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NCMB;

public class AutoLogin : MonoBehaviour
{
    private const string userNameKey = "userName";//_user_name_
    private const string passwordKey = "password"; //_pass_word_
    public InputField usernameinputField;
    private bool isLogin = false;
    public static bool isOnLogin = false;
   


    //---------------------------------------------------------------------------------------------
    // アプリ起動時に呼ばれるメソッド (オートログイン)
    //---------------------------------------------------------------------------------------------
    IEnumerator Start()
    {
        // Unity(端末)に保存された `ユーザ名` と `パスワード` を取得 (保存されていない場合は空文字で初期化)
        string userName = PlayerPrefs.GetString(userNameKey, "");
        string password = PlayerPrefs.GetString(passwordKey, "");

        // Unityにユーザ情報が保存されているか
        if (userName == "")
        {
            // 保存なし -> ユーザ登録処理
            Debug.Log("ユーザーデータが存在しません");
            yield return StartCoroutine(userRegistrationCoroutine());

        }
        else
        {
            // 保存あり -> 現在ログインしているか
            if (NCMBUser.CurrentUser == null)
            {
                // ログインしていない -> ログイン処理
                yield return StartCoroutine(loginCoroutine(userName, password));
            }
            else
            {
                Debug.Log("ログインしました");
                isOnLogin = true;

            }
        }
    }

    //---------------------------------------------------------------------------------------------
    // 任意文字数のランダム文字列を生成するメソッド
    //---------------------------------------------------------------------------------------------
    const string letters = "1234567890";
    private string genRandomString(uint strLength)
    {
        string randStr = "";
        for (int i = 0; i < strLength; i++)
        {
            char randLetter = letters[Mathf.FloorToInt(Random.value * letters.Length)];
            randStr += randLetter;
        }
        return randStr;
    }

    //---------------------------------------------------------------------------------------------
    // 会員登録を行うメソッド
    //---------------------------------------------------------------------------------------------
    public void isLoginName()
    {
        isLogin = true;
    }
    private IEnumerator userRegistrationCoroutine()
    {

        Debug.Log("ログイン中");
        // NCMBUserクラスのインスタンス生成
        NCMBUser user = new NCMBUser();

        // パスワード
        string password = genRandomString(16);
        user.Password = password;

        // UserIDが重複したらやり直し


        bool isSuccess = false;
        while (!isSuccess)
        {
            bool isConnecting = true;
            string inputValue = usernameinputField.text;
            // ユーザ名を設定してユーザ登録実行（非同期処理）
            if (isLogin)
            {
                user.UserName = inputValue;

            }
            user.SignUpAsync((NCMBException e) =>
                {
                    if (e != null)
                    {
                    // ユーザ登録失敗
                    if (e.ErrorCode != NCMBException.DUPPLICATION_ERROR)
                        {
                        // ユーザ名重複以外のエラーが発生
                        //Debug.Log("Failed to registerate: " + e.ErrorMessage);
                        Debug.Log("ユーザー名が既に登録されています");
                        }
                    }
                    else
                    {
                    // ユーザ登録成功
                    isSuccess = true;
                        Debug.Log("ユーザーデータを登録しました");
                    // Unity(端末)に情報を設定
                    PlayerPrefs.SetString(userNameKey, user.UserName);
                        PlayerPrefs.SetString(passwordKey, password);
                    // カレントユーザー情報の取得
                    NCMBUser currentUser = NCMBUser.CurrentUser;
                    //ログインユーザー名表示
                    UnityEngine.Debug.Log(user.UserName);
                        if (currentUser != null)
                        {
                        // クラスのNCMBObjectを作成
                        NCMBQuery<NCMBObject> query = new NCMBQuery<NCMBObject>("UserData");
                            query.WhereEqualTo("user_name", user.UserName);
                            query.FindAsync((List<NCMBObject> objList, NCMBException a) =>
                                {
                                    if (a == null)
                                    {
                                        if (objList.Count == 0)
                                        {
                                          //検索失敗時の処理
                                          //初回ユーザーデータ登録
                                          NCMBObject UserData = new NCMBObject("UserData");
                                          UserData["user_name"] = user.UserName;
                                          UserData["user_rank"] = 1;
                                          // データストアへの登録
                                          UserData.SaveAsync();
                                          isOnLogin = true;
                                          UnityEngine.Debug.Log("ユーザーデータを保存しました");
                                          return;
                                        }
                                        else
                                        {
                                            UnityEngine.Debug.Log("ユーザーデータがすでに存在しています");

                                        }
                                    }
                                    else
                                    {
                                        UnityEngine.Debug.Log("ユーザーデータがすでに存在しています");

                                    }
                                });
                        }
                        else
                        {

                            UnityEngine.Debug.Log("未ログインまたは取得に失敗");
                        }

                    }
                // ユーザ登録処理（１ループ）終了
                isConnecting = false;
                });
            
            // ユーザ登録処理（１ループ）が終了するまで以下の行でストップ（強制的に同期処理にする）
            yield return new WaitWhile(() => { return isConnecting; });
        }
    }




    public void LogOut()
    {
        try
        {
            NCMBUser.LogOutAsync();
            // Unity(端末)に情報を設定
            PlayerPrefs.SetString(userNameKey, "");
            PlayerPrefs.SetString(passwordKey, "");
        }
        catch (NCMBException e)
        {
            UnityEngine.Debug.Log("エラー: " + e.ErrorMessage);
        }

    }



    //---------------------------------------------------------------------------------------------
    // ログインを行うメソッド
    //--------------------------------------------------------------------------------------------- 
    private IEnumerator loginCoroutine(string userName, string password)
    {
        bool isConnecting = true;
        NCMBUser.LogInAsync(userName, password, (NCMBException e) =>
        {
            if (e != null)
            {
                // ログイン失敗
                //Debug.Log("Failed to login: " + e.ErrorMessage);
                Debug.Log("ログインに失敗しました");

            }
            else
            {
                // ログイン成功
                Debug.Log("ログインしました");
                isOnLogin = true;

            }
            // ログイン処理終了
            isConnecting = false;
        });
        // ログイン処理が終了するまで以下の行でストップ
        yield return new WaitWhile(() => { return isConnecting; });
    }
}
