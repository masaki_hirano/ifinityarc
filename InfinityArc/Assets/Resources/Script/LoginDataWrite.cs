﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using NCMB;

public class LoginDataWrite : MonoBehaviour {

    public Text _UserName;
    public Text _UserRank;
    private string s_UserName;
    private string s_UserRank;
    bool isDataInput = false;


    private const string userNameKey = "userName";//_user_name_

    // Use this for initialization
    void Start () {

    }

    //Update is called once per frame

    void Update()
    {
        if (AutoLogin.isOnLogin)
        {
            //Unity(端末)に保存された `ユーザ名` を取得(保存されていない場合は空文字で初期化)

            // 保存あり -> 現在ログインしているか
            if (NCMBUser.CurrentUser == null)
            {
                Debug.Log("ログインしていません");
                //return;
            }
            else
            {

                if (!isDataInput)
                {
                    //UserDataを検索するクラスを作成
                    //NCMBObject UserData = new NCMBObject("UserData");
                    NCMBQuery<NCMBObject> query = new NCMBQuery<NCMBObject>("UserData");
                    //user_nameの値が"保存されているユーザー名"と一致するオブジェクト検索
                    query.WhereEqualTo("user_name", PlayerPrefs.GetString(userNameKey, ""));
                    query.FindAsync((List<NCMBObject> objList, NCMBException e) =>
                    {
                        if (e == null)
                        {

                            foreach (NCMBObject obj in objList)
                            {
                                s_UserName = System.Convert.ToString(obj["user_name"]);
                                s_UserRank = System.Convert.ToString(obj["user_rank"]);
                            //Debug.Log(System.Convert.ToString(obj["user_name"]));
                            //Debug.Log(System.Convert.ToString(obj["user_rank"]);
                            isDataInput = true;
                            AutoLogin.isOnLogin = false;
                                return;
                            }

                        }
                        else
                        {
                        //検索失敗時の処理
                        return;

                        }
                    });
                }

            }


            TextWrite();
        }

    }

    void TextWrite()
    {
        _UserName.text = "ログイン名：" +  s_UserName;
        _UserRank.text = "ランク　　：" +  s_UserRank;
    }
}
